package com.example.c00p3r.tweetmate.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.c00p3r.tweetmate.R;

public class SendVoiceFragment extends Fragment {
  
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View voiceFragment= inflater.inflate(R.layout.fragment_send_voice, container, false);
        return voiceFragment;
    }

}
