package com.example.c00p3r.tweetmate.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.c00p3r.tweetmate.R;


public class TimeLineFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Button signin;
        View tlFragment=inflater.inflate(R.layout.fragment_time_line, container, false);
        signin=(Button)tlFragment.findViewById(R.id.sign_in_button);
       signin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
               builder.setCancelable(true);
               builder.setTitle("توجه");
               builder.setIcon(R.drawable.ic_assignment_late_red_600_24dp);
               builder.setMessage("این یه دروغ اول آوریل یا همون دروغ سیزده بود و متاسفانه شما سرکار رفتین! هیچ داده ای اینجا رد و بدل نشده و این فقط یه شوخی بود :)");
               builder.setPositiveButton("ای بابا",
                       new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialog, int which) {
                           }
                       });
               AlertDialog dialog = builder.create();
               dialog.show();
           }
       });
        return tlFragment;
    }
    }